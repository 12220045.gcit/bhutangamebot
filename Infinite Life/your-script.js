// Get mission and vision elements
const missionSection = document.querySelector('.mission-section');
const visionSection = document.querySelector('.vision-section');

// Get mission and vision buttons
const missionBtn = document.getElementById('missionBtn');
const visionBtn = document.getElementById('visionBtn');

// Get back buttons
const backBtnMission = document.getElementById('backBtnMission');
const backBtnVision = document.getElementById('backBtnVision');

// Function to show mission section
function showMissionSection() {
    missionSection.classList.remove('hidden');
    document.body.classList.add('overflow-hidden'); // Prevent scrolling when popup is open
}

// Function to show vision section
function showVisionSection() {
    visionSection.classList.remove('hidden');
    document.body.classList.add('overflow-hidden'); // Prevent scrolling when popup is open
}

// Function to hide mission and vision sections
function hideSections() {
    missionSection.classList.add('hidden');
    visionSection.classList.add('hidden');
    document.body.classList.remove('overflow-hidden'); // Allow scrolling when popup is closed
}

// Add event listeners to the mission and vision buttons
missionBtn.addEventListener('click', showMissionSection);
visionBtn.addEventListener('click', showVisionSection);

// Add event listeners to the back buttons
backBtnMission.addEventListener('click', hideSections);
backBtnVision.addEventListener('click', hideSections);
